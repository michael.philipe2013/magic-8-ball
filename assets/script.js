let portugueseInstruction =
  "Faça uma pergunta para obter uma resposta de sim ou não";

let englishInstruction =
  "ask a yes or no question and click on the ball to get an answer";

const SPAN = document.createElement("div");
const ANSWER = document.createElement("div");
const CHANGE_IDIOM = document.getElementById("change-idiom");
const INSTRUCTION = document.getElementById("instruction");
INSTRUCTION.innerText = englishInstruction;
ANSWER.id = "answer";
SPAN.classList.add("disappear");
const MAIN = document.getElementById("main");
MAIN.appendChild(ANSWER);
const BOLA = document.getElementById("main-img");
ANSWER.appendChild(SPAN);

//ENGLISH

const ANSWERS = [
  "It is certain.",
  "It is decidedly so.",
  "Without a doubt.",
  "Yes – definitely.",
  "You may rely on it.",
  "As I see it, yes.",
  "Most likely.",
  "Outlook good.",
  "Yes.",
  "Signs point to yes.",
  "Reply hazy, try again.",
  "Ask again later.",
  "Better not tell you now.",
  "Cannot predict now.",
  "Concentrate and ask again.",
  "Don't count on it.",
  "My reply is no.",
  "My sources say no.",
  "Outlook not so good.",
  "Very doubtful.",
];
//PORTUGUESE

let PORTUGUESE_ANSWERS = [
  "É certo.",
  "É decididamente assim.",
  "Sem dúvida.",
  "Sim - definitivamente.",
  "Você pode confiar nisso.",
  "A meu ver, sim.",
  "Provavelmente.",
  "Parece bom.",
  "Sim.",
  "Os sinais apontam para sim.",
  "Resposta nebulosa, tente novamente.",
  "Pergunte novamente mais tarde.",
  "Melhor não contar agora.",
  "Não é possível prever agora.",
  "Concentre-se e pergunte novamente.",
  "Não conte com isso.",
  "Minha resposta é não.",
  "Minhas fontes dizem não.",
  "Não me parece bom.",
  "Muito duvidoso.",
];
console.log(PORTUGUESE_ANSWERS.length);

//GERAR RAMDOM NUMBER
function getRandomInt(answersLenght) {
  return Math.floor(Math.random() * Math.floor(answersLenght));
}

function insertAnswers(answers) {
  SPAN.innerText = `${answers[getRandomInt(answers.length)]}`;
}

function giraBall() {
  return BOLA.classList.toggle("gira");
}
function appear() {
  SPAN.classList.remove("appear");
  setTimeout(function () {
    SPAN.classList.add("appear");
  }, 1000);
}

BOLA.addEventListener("click", function () {
  if (CHANGE_IDIOM.className === "english") {
    insertAnswers(ANSWERS);
  } else {
    insertAnswers(PORTUGUESE_ANSWERS);
  }

  giraBall();
  appear();
});
CHANGE_IDIOM.addEventListener("click", function () {
  if (CHANGE_IDIOM.className === "english") {
    CHANGE_IDIOM.className = "portuguese";
    INSTRUCTION.innerText = `${portugueseInstruction}`;
    CHANGE_IDIOM.innerText = "english";
  } else {
    CHANGE_IDIOM.className = "english";
    INSTRUCTION.innerText = `${englishInstruction}`;
    CHANGE_IDIOM.innerText = "portuguese";
  }
});
